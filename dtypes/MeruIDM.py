import re

# MeruIDM
# Devices > Syslog Servers add action=start, action=interim, action=stop to the 3 lines
name = "MeruIDM"
protocol = 'syslog'

messagere = re.compile("action=(\S+).*name=([\S ]+)\smac.*ip=(\d+\.\d+\.\d+\.\d+)")

def handle(device, message):
    mo = messagere.search(message)
    if not mo:
        return
    a = mo.group(1)
    u = mo.group(2)
    i = mo.group(3)

    if a == "start":
        a = "login"
    elif a == "stop":
        a = "logout"
    elif a == "interim":
        a = "login"
    else:
        return None

    u = u.replace("\\\\", "\\")
    u = u.lower()

    return (a,u,i,None)
