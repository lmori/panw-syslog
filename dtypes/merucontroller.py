name = 'MeruController'
protocol = 'radius'

def handle(device, pkt):
	if not pkt.has_key('Framed-IP-Address') or not pkt.has_key('User-Name'):
		return None
	fip = pkt['Framed-IP-Address'][0]
	un = pkt['User-Name'][0]
	if not pkt.has_key('Acct-Status-Type'):
		return None
	if pkt['Acct-Status-Type'][0] == 'Stop':
		a = 'logout'
	else:
		a = 'login'

	un = un.replace("\\\\", "\\")
	un = un.lower()
	
	return (a, un, fip, None)
