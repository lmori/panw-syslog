"""
Takes the entries from the queue, build the requests and send
them to the agent.
"""

from twisted.internet.protocol import Protocol, ReconnectingClientFactory
from twisted.internet import reactor, ssl, defer
from twisted.internet import task
from twisted.protocols import basic
import re
import inspect
import time
import log

defer.setDebugging(True)

class UserIDAgentAPI(Protocol):
    def __init__(self):
        self.connected = False

        # keepalive each 5 mins
        self.keepalive_task = task.LoopingCall(self.keepalive)
        self.ur_re = re.compile("<code>(\d+)</code><message>(\S+)</message>")

    def keepalive(self):
        if not self.connected:
            return
        self.send_update_request()
        log.debug("Keepalive sent")

    def dataReceived(self, data):
        self.answ_buffer = self.answ_buffer+data
        if self.answ_buffer.endswith('</uid-response>'):
            log.debug(self.answ_buffer)
            self.outstanding_request = False

            if self.outstanding_deferred:
                mo = self.ur_re.search(self.answ_buffer)
                if not mo:
                    cbargs = (None, None)
                else:
                    cbargs = (mo.group(1), mo.group(2))
            reactor.callLater(0, self.outstanding_deferred.callback, cbargs) # XXX callLater really needed ???
            self.outstanding_deferred = None
            self.answ_buffer = ""

    def send_update_request(self, llogin=None, llogout=None):
        if not self.connected:
            return
        if self.outstanding_request:
            return 'BUSY'

        msglines = []
        msglines.append("<uid-message><version>1.0</version><type>update</type><payload>")
        if llogin and len(llogin) != 0:
            msglines.append("<login>")
            for l in llogin:
                msglines.append('<entry name="%s" ip="%s"/>'%l[:2])
            msglines.append("</login>")
        if llogout and len(llogout) != 0:
            msglines.append("<logout>")
            for l in llogout:
                msglines.append('<entry name="%s" ip="%s"/>'%l[:2])
            msglines.append("</logout>")
        msglines.append("</payload></uid-message>")
        request = "".join(msglines)

        self.transport.write(request.encode('utf-8'))
        self.outstanding_request = True
        self.outstanding_deferred = defer.Deferred()

        return self.outstanding_deferred

    def connectionMade(self):
        self.connected = True
        self.answ_buffer = ""
        self.outstanding_request = False
        self.outstanding_deferred = None

        # start keepalive
        self.keepalive_task.start(300.0)

    def connectionLost(self, reason):
        self.connected = False
        # stop keepalive
        self.keepalive_task.stop()

class UserIDAgentAPIFactory(ReconnectingClientFactory):
    def __init__(self, name):
        self.agentname = name
        self.active_protocol = None
        self.maxDelay = 300

    def is_connected(self):
        return (self.active_protocol and self.active_protocol.connected)

    def buildProtocol(self, addr):
        self.resetDelay()
        if not self.active_protocol:
            self.active_protocol = UserIDAgentAPI()
            self.active_protocol.factory = self
        return self.active_protocol

    def clientConnectionLost(self, connector, reason):
        self.active_protocol = None
        log.err("Agent connect lost: %s"%self.agentname)
        ReconnectingClientFactory.clientConnectionLost(self, connector, reason)

    def clientConnectionFailed(self, connector, reason):
        self.active_protocol = None
        log.err("Agent connect failed: %s"%self.agentname)
        ReconnectingClientFactory.clientConnectionFailed(self, connector, reason)

class PanwUpdate:
    def __init__(self, agentlist, queue):
        self.agentlist = agentlist
        self.queue = queue

        self.updates_to_agent = []
        self.poll_deferred = None

        self.agents = []
        for a in agentlist:
            f = UserIDAgentAPIFactory(a[0])
            reactor.connectSSL(a[1], a[2], f, ssl.ClientContextFactory())
            self.agents.append(f)

    def __push_result(self, res):
        code = res[0]
        msg = res[1]
        log.debug("Push result %s - %s"%(code, msg))
        if msg == 'ok':
            d = self.queue.set_ids_done(self.outstanding_ids)
            d.addCallback(self.__push)
            d.addErrback(self.__handle_error)
        else:
            log.err('Error from Agent %s'%msg)
            d = task.deferLater(reactor, 0, self.__push)
            d.addErrback(self.__handle_error)

    def __send_update_request(self, ap, llogin, llogout):
        log.debug("Push login: %s logout: %s"%(repr(llogin), repr(llogout)))
        r = ap.send_update_request(llogin, llogout)
        if not r:
            log.err("agent not connected")
            self.poll_deferred.callback('agent not connected')
            return
        if type(r) == str and r == 'BUSY':
            log.debug("Push result: BUSY")
            d = task.deferLater(reactor, 1, self.__send_update_request, ap, llogin, llogout)
            d.addErrback(self.__handle_error)
            return
        
        r.addCallback(self.__push_result)
        r.addErrback(self.__handle_error)

    def __push(self, a=None):
        if len(self.updates_to_agent) == 0:
            self.poll_deferred.callback('ok')
            return

        ap = None
        for a in self.agents:
            if a.is_connected():
                ap = a.active_protocol
                break
        if not ap:
            self.poll_deferred.callback('no active agents')
            return

        llogin = []
        llogout = []
        outstanding_ids = []
        for i in range(50):
            if len(self.updates_to_agent) == 0:
                break
            e = self.updates_to_agent.pop()

            if not 'action' in e:
                outstanding_ids = outstanding_ids+e['ids']
                continue

            if e['action'] == 'login':
                llogin.append((e['username'], e['ip']))
                outstanding_ids = outstanding_ids+e['ids']
            elif e['action'] == 'logout':
                llogout.append((e['username'], e['ip']))
                outstanding_ids = outstanding_ids+e['ids']
            else:
                outstanding_ids = outstanding_ids+e['ids']

        self.outstanding_ids = outstanding_ids

        self.__send_update_request(ap, llogin, llogout)

    # (id, timestamp, action, username, ip, ttl)
    def __analyze_and_push(self, r):
        if not r or len(r) == 0:
            self.poll_deferred.callback('none')
            return

        ipentries = {}  
        now = time.time()

        for e in r:
            if not e[4] in ipentries:
                ipentries[e[4]] = {'ids': []}
            ipentries[e[4]]['ids'].append(e[0])
            if e[2] == "login":
                if e[1]+e[5] < now:
                    continue
                ipentries[e[4]]['action'] = 'login'
                ipentries[e[4]]['ip'] = e[4]
                ipentries[e[4]]['username'] = e[3]
            elif e[2] == 'logout':
                ipentries[e[4]]['action'] = 'logout'
                ipentries[e[4]]['ip'] = e[4]
                ipentries[e[4]]['username'] = e[3]

        self.updates_to_agent = ipentries.values()

        d = task.deferLater(reactor, 0, self.__push)
        d.addErrback(self.__handle_error)

    def __handle_error(self, e):
        self.poll_deferred.callback('error')
        log.err(e.getErrorMessage())

    def poll(self):
        # poll will be called again only when we fire
        # poll_deferred, this is by definition of LoopingCall
        self.poll_deferred = defer.Deferred()

        # fetach all on the queue and start pushing
        # the results
        fad = self.queue.fetch_all_new()
        fad.addCallback(self.__analyze_and_push)
        fad.addErrback(self.__handle_error)

        return self.poll_deferred
