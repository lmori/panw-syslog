from twisted.enterprise.adbapi import ConnectionPool
from twisted.internet import defer
from twisted.python import log
from twisted.internet import reactor

import sqlite3
import time

class UpdateQueue:
    add_query = "INSERT INTO updatequeue (timestamp,action,ip,username,ttl,status) VALUES (?,?,?,?,?,?)"

    def __init__(self, dbname, ttldefault):
        self.__init_db(dbname)
        self.__db_pool = ConnectionPool('sqlite3', dbname, check_same_thread=False)
        self.ttldefault = ttldefault
        self.cache = {}
        self.add_queue = []

    def __init_db(self, dbname):
        conn = sqlite3.connect(dbname)
        cursor = conn.cursor()

        cursor.execute('CREATE TABLE IF NOT EXISTS updatequeue (id INTEGER PRIMARY KEY AUTOINCREMENT, timestamp INTEGER, action TEXT, ip TEXT, username TEXT, ttl INTEGER, status TEXT)')
        conn.commit()
        cursor.close()
        conn.close()

    def __work_on_add_queue(self, f = None, e = None):
        if len(self.add_queue) == 0:
            return
        if not e:
            e = self.add_queue.pop(0)
        self.cache = {}
        d = self.__db_pool.runQuery(self.add_query, e)
        d.addCallback(self.__work_on_add_queue)
        d.addErrback(self.__work_on_add_queue, e)

    def add(self, action, ip, username, ttl=None):
        if not ttl:
            ttl = self.ttldefault
        now = time.time()
        self.add_queue.append((now, action, ip, username, int(ttl), 'N'))
        reactor.callLater(0, self.__work_on_add_queue)

    def set_ids_done(self, ids):
        def __set_ids_done(txn, ids):
            for i in ids:
                txn.execute("UPDATE updatequeue SET status='D' WHERE id=?", (i,))
            return 'ok'
        self.cache = {}
        return self.__db_pool.runInteraction(__set_ids_done, ids)

    def fetch_all_new(self):
        def __fetch_all_new(txn, q):
            txn.execute("SELECT * FROM updatequeue WHERE status='N' ORDER BY id")
            result = txn.fetchall()
            if result:
                q.cache['fetch_all_new'] = result
                return result
            else:
                q.cache['fetch_all_new'] = None
                return None

        if 'fetch_all_new' in self.cache:
            d = defer.Deferred()
            d.callback(self.cache['fetch_all_new'])
            return d

        return self.__db_pool.runInteraction(__fetch_all_new, self)

    def shutdown(self):
        self.__db_pool.close()
