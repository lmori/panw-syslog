"""
Really simple Syslog Server implementation.
Receives the UDP datagrams, pass them to a dtype module
based on the client table and then store the result
into the queue.
"""

from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
from pyrad import packet, dictionary

import log

class SyslogServer(DatagramProtocol):
    def __init__(self, client_table, queue, lm):
        self.client_table = client_table
        self.queue = queue
        self.lm = lm

    def datagramReceived(self, data, (host, port)):
        if host in self.client_table:
            client = self.client_table[host]
            cres = client[2]['module'].handle(client[0], data)
            if cres and self.lm:
                cres = self.lm.handle(client[0], client[1], cres)
            if cres:
                log.debug('%s - %s - %s'%(host, data, repr(cres)))
                self.queue.add(cres[0], cres[1], cres[2], cres[3])
            else:
                log.debug('%s - %s - %s'%(host, data, 'unhandled'))
        else:
            log.err('received msg from unknown host %s - %s'%(host, data))

class RadiusServer(DatagramProtocol):
    def __init__(self, dfilename, client_table, queue, lm):
        self.client_table = client_table
        self.queue = queue
        self.radius_dict = dictionary.Dictionary(dfilename)
        self.lm = lm

    def datagramReceived(self, data, (host, port)):
        if host in self.client_table:
            client = self.client_table[host]
            pkt = packet.AcctPacket(dict=self.radius_dict, packet=data)
            if pkt.code != packet.AccountingRequest:
                log.err('receieved martian RADIUS packet with code: %d'%(pkt.code))
                return

            r = []
            for k in pkt.keys():
                r.append("%s: %s"%(k, pkt[k]))
            log.debug('packet from %s: %s'%(host, ','.join(r)))

            pkt.secret = client[2]
            if not pkt.VerifyAcctRequest():
                log.err('Acct packet from %s - authentication failed'%(host))
                return

            rpkt = pkt.CreateReply()
            self.transport.write(rpkt.ReplyPacket(), (host, port))
            
            cres = client[3]['module'].handle(client[0], pkt)
            if cres and self.lm:
                cres = self.lm.handle(client[0], client[1], cres)
            if cres:
                log.debug('%s - %s'%(host, repr(cres)))
                self.queue.add(cres[0], cres[1], cres[2], cres[3])
            else:
                log.debug('%s - %s'%(host, 'unhandled'))
        else:
            log.err('received msg from unknown host %s'%(host))

