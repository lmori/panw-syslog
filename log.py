from twisted.python.logfile import LogFile
from twisted.python import log
import logging

__observer = None

class LittleLogFile(LogFile):
    def _openFile(self):
        self.closed = False
        self._file = file(self.path, "a", 1)
        self.size = self._file.tell()

class LevelFileLogObserver(log.FileLogObserver):
	def __init__(self, f, level=logging.INFO):
		log.FileLogObserver.__init__(self, f)
		self.logLevel = level

	def emit(self, eventDict):
		if eventDict['isError']:
			level = logging.ERROR
		elif 'level' in eventDict:
			level = eventDict['level']
		else:
			level = logging.INFO
		if level >= self.logLevel:
			log.FileLogObserver.emit(self, eventDict)

def init(lf, level=logging.INFO):
	lf = LittleLogFile.fromFullPath(lf, 100000000)
	__observer = LevelFileLogObserver(lf, level)
	log.addObserver(__observer.emit)

def info(msg):
	log.msg("[INFO] "+msg, level=logging.INFO)
msg = info

def error(msg):
	log.err("[ERROR] "+msg)
err = error

def debug(msg):
	log.msg("[DEBUG] "+msg, level=logging.DEBUG)

