"""
main file for panw-syslog
"""

from twisted.internet import reactor
from twisted.internet import task
from twisted.python.failure import Failure
import ConfigParser
import imp
import os
import logging

import server
import panwupdate
import log
import queue

config = ConfigParser.ConfigParser()
utask = None

def find_dtypes_modules(base_dir):
    # load the modules from the dtypes subdirectory
    # import them and build a dtype->imp mapping
    dtdir = os.path.join(base_dir, "dtypes")
    dtypes = {}
    
    dtmodules = set()
    for dtfile in os.listdir(dtdir):
        module = None
        if dtfile.endswith(".py"):
            dtmodules.add(dtfile[:-3])
        elif dtfile.endswith(".pyc"):
            dtmodules.add(dtfile[:-4])
    dtmodules = list(dtmodules)

    for dt in dtmodules:
        log.msg('loading '+dt)
        (f, p, d) = imp.find_module(dt, [dtdir])
        dtm = imp.load_module(dt, f, p, d)
        dtypes[dtm.name] = { 'module': dtm, 'protocol': dtm.protocol }
    return dtypes

def load_local_if_exists(base_dir):
    log.msg("loading local.py")
    try:
        (f,p,d) = imp.find_module("local", [base_dir])
        lm = imp.load_module("local", f, p, d)
    except:
        f = Failure().getErrorMessage()
        log.err("Error loading local.py: "+f)
        return None

    return lm

def build_client_tables(dtypes):
    # build a table mapping syslog client IP -> name,dtype
    radiusct = {}
    syslogct = {}

    for s in config.sections():
        if s in ['main', 'syslog', 'radius']:
            continue
        p = config.get(s, 'protocol')
        a = config.get(s, 'address')
        t = config.get(s, 'type')
        if t in dtypes:
            if dtypes[t]['protocol'] != p:
                log.err('protocol mismatch in section %s'%(s))
                continue
            if p == 'radius':
                secret = config.get(s, 'secret')
                radiusct[a] = (s, t, secret, dtypes[t])
            elif p == 'syslog':
                syslogct[a] = (s, t, dtypes[t])
            else:
                log.err('Unknown protocol %s in section %s'%(p, s))
        else:
            log.err('Unknown device type: %s'%t)
    return syslogct, radiusct

def parse_agent_list(al):
    # parse the agent list from config file
    res = []
    al = al.strip()
    agents = al.split(',')
    for a in agents:
        if len(a) == 0:
            continue
        a = a.strip()
        n,a,p = a.split(':')
        res.append((n,a,int(p)))
    return res

def get_abs_filename(base_dir, fname):
    if os.path.isabs(fname):
        return fname
    return os.path.join(base_dir, fname)

def main(base_dir):
    logleveldict = {
        'info': logging.INFO,
        'debug': logging.DEBUG,
        'error': logging.ERROR,
        'warning': logging.WARNING,
        'critical': logging.CRITICAL
    }

    # read the config
    config.read(get_abs_filename(base_dir, "panw-syslog.cfg"))
    ll = config.get('main', 'loglevel')
    if not ll in logleveldict:
        print 'Unknown loglevel %s - reset to "info"'%ll
        ll = 'info'
    ll = logleveldict[ll]

    # open logging
    lf = get_abs_filename(base_dir, config.get('main', 'logfile'))
    log.init(lf, ll)

    # load dtypes
    dtypes = find_dtypes_modules(base_dir)

    # load local.py
    local = load_local_if_exists(base_dir)

    # build IP -> dtype table
    syslog_client_table, radius_client_table = build_client_tables(dtypes)

    # create the queue
    uqueue = queue.UpdateQueue(get_abs_filename(base_dir, config.get('main', 'dbfilename')), config.get('main', 'defaulttl'))

    # create the UDP Syslog Server
    ss = server.SyslogServer(syslog_client_table, uqueue, local)
    reactor.listenUDP(int(config.get('syslog', 'port')), ss)

    # build RADIUS clients table
    rs = server.RadiusServer(get_abs_filename(base_dir, config.get('radius', 'dictionary')), radius_client_table, uqueue, local)
    reactor.listenUDP(int(config.get('radius', 'port')), rs)

    # create the polling loop
    al = parse_agent_list(config.get('main', 'agentlist'))
    pu = panwupdate.PanwUpdate(al, uqueue)
    utask = task.LoopingCall(pu.poll)
    utask.start(1.0)

    # go
    reactor.run()

if __name__ == "__main__":
    base_dir = os.path.dirname(os.path.realpath(__file__))
    main(base_dir)
