# PANW-SYSLOG

panw-syslog is a dumb, little syslog server that can be used to interface 3rd party devices with the User-ID XML API of Palo Alto Networks.

## INSTALLATION

### WINDOWS
- install [Python 2.7](http://www.python.org/getit/)
- install [Twisted](http://twistedmatrix.com/trac/wiki/Downloads)
- install [Zope.Interface](http://pypi.python.org/pypi/zope.interface#download) (required by Twisted)
- install [pyOpenSSL](https://pypi.python.org/pypi/pyOpenSSL) (required by Twisted)
- install [pyrad](https://pypi.python.org/pypi/pyrad)
- download and decompress panw-syslog source files in a directory, for example c:\panw-syslog
- check and modify panw-syslog.cfg config file
- download [NSSM](http://nssm.cc/download/) and extract nssm.exe in the same directory
- run 'nssm install panw-syslog c:\Python27\python.exe c:\panw-syslog\main.py' to create panw-syslog service

### LINUX
TBD

## DISCLAIMER
The code is ugly and dirty, don't look at it. Or clean it and let me know.

## COPYRIGHT
Copyright (c) 2013 Luigi Mori

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
