"""
Python syslog client.

This code is placed in the public domain by the author.
Written by Christian Stigen Larsen.

This is especially neat for Windows users, who (I think) don't
get any syslog module in the default python installation.

See RFC3164 for more info -- http://tools.ietf.org/html/rfc3164

Note that if you intend to send messages to remote servers, their
syslogd must be started with -r to allow to receive UDP from
the network.
"""

import socket
import time
import random

# I'm a python novice, so I don't know of better ways to define enums

FACILITY = {
'kern': 0, 'user': 1, 'mail': 2,
'daemon': 3,'auth': 4, 'syslog': 5, 
'lpr': 6, 'news': 7,'uucp': 8, 'cron': 9, 
'authpriv': 10, 'ftp': 11, 'local0': 16,
'local1': 17, 'local2': 18, 'local3': 19,
'local4': 20, 'local5': 21, 'local6': 22,
'local7': 23
}

LEVEL = {
'emerg': 0, 'alert':1, 'crit': 2, 'err': 3,
'warning': 4, 'notice': 5, 'info': 6, 'debug': 7
}

actions = ['start', 'stop']
users = ['pinco@foobar.com', 'pallo@foobar.com', 'tizio@foobar.com', 'caio@foobar.com', "host/prova.example.com"]
ips = ['60', '61', '62', '63', '64', '65', '66']

def syslog(message, level=LEVEL['notice'], facility=FACILITY['daemon'],
    host='localhost', port=514):

    """
    Send syslog UDP packet to given host and port.
    """

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    data = '<%d>%s' % (level + facility*8, message)
    sock.sendto(data, (host, port))
    sock.close()

if __name__ == "__main__":
    random.seed()
    while True:
        syslog("Jul 22 15:50:48 meruidm01 radiusd: action=%s name=%s mac=D8-9E-3F-9B-9A-80 ip=192.168.30.%s $"%(actions[random.randrange(0, len(actions))], users[random.randrange(0, len(users))], ips[random.randrange(0, len(ips))]), LEVEL['info'], FACILITY['user'])
        time.sleep(1.0)
